from django.shortcuts import render, get_object_or_404

# Create your views here.

from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.views import generic
from .models import Choice, Question

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template('sondage/index.html')  
    context = {'latest_question_list':  latest_question_list, }
    return HttpResponse(template.render(context, request))

def detail(request, question_id):
    try:
         question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, 'sondage/detail.html', {'question': question})

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'sondage/results.html', {'question': question})

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
       #selected_choice = question.choice_set.get(pk=request.POST['choice'])
       answers = request.POST.getlist('choice')
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'sondage/detail.html', {
            'question': question,
            'error_message': "Vous n'avez pas fait de choix",
        })
    else:  
        print(answers)
        question.choise_set.all())
        for each_choice in question.choise_set.all():
            print(each_choise.id)
            if each_choise.is good == True:
                if str(each_choise.id) in answers:
                    continue
                else:
                    print("Mauvaise réponse")
        #selected_choice.votes += 1
        #selected_choice.save()
        return HttpResponseRedirect(reverse('sondage:results', args=(question.id,)))



